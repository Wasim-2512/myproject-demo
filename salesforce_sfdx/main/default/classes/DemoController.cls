public with sharing class DemoController {
    /**
     * An empty constructor for the testing1
     */
    public DemoController() {}

    /**
     * Get the version of the SFDX demo app2 to check vlocity yes tool
     */

    public String getAppVersion() {
        return '1.0.0';
    }
}
//test
